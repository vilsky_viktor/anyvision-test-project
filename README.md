# AnyVision test project

1. Clone project to your local directory
2. There are 2 parts: back-end & front-end. They sit in folders server and client respectively. Go to root folders of both projects and run npm install
3. Make sure mongo DB is installed or you have access to cloud DB
4. Copy .env.example files for both projects and paste as .env filled with your credentials
5. Run both projects on their root folders via npm start

That's it, have fun :)
